let validator = new Validation();

document.querySelector("#userId").style.display = "none";

renderListOfUsers();

function deleteUser(id) {
  turnOnLoading();
  userService
    .deleteData(id)
    .then((res) => {
      turnOffLoading();
      renderListOfUsers();
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
}

function editUser() {
  let editUser = getInforFromForm();
  let id = document.querySelector("#userId span").innerHTML;
  userService
    .getList()
    .then((res) => {
      let userList = res.data;
      let isValidAccount;
      userList.forEach((item) => {
        console.log(item);
        if (editUser.account == item.account && id != item.id) {
          console.log(id, item.id);
          isValidAccount = false;
          document.getElementById("tbTaiKhoan").innerText =
            "Tài khoản đã tồn tại";
        } else {
          isValidAccount = true;
          document.getElementById("tbTaiKhoan").innerText = "";
        }
      });
      let isValid = validateInput(userList) && isValidAccount;
      if (isValid) {
        userService
          .changeData(editUser, id)
          .then((res) => {
            renderListOfUsers();
            $("#myModal").modal("hide");
          })
          .catch((err) => {});
      }
    })
    .catch((err) => {});
}

function addNewUser() {
  userService
    .getList()
    .then((res) => {
      let isValidAccount =
        validator.kiemTraRong(
          "TaiKhoan",
          "tbTaiKhoan",
          "Tài khoản không được để trống"
        ) && validator.kiemTaiKhoanHopLe(res.data);
      let isValid = isValidAccount & validateInput(res.data);
      if (isValid) {
        let newUser = getInforFromForm();
        axios({
          url: `${BASE_URL}/user`,
          method: "POST",
          data: newUser,
        })
          .then((res) => {
            $("#myModal").modal("hide");
            renderListOfUsers();
          })
          .catch((err) => {
            console.log(err);
          });
      }
    })
    .catch((err) => {});
}

document.getElementById("btnThemNguoiDung").addEventListener("click", () => {
  document.getElementById("addBtn").style.display = "block";
  document.getElementById("editBtn").style.display = "none";
  document.querySelector("#userId").style.display = "none";
  document.getElementById("form-user").reset();
  document.getElementById("tbTaiKhoan").innerText = "";
  document.getElementById("tbHoTen").innerText = "";
  document.getElementById("tbMatKhau").innerText = "";
  document.getElementById("tbEmail").innerText = "";
  document.getElementById("tbHinhAnh").innerText = "";
  document.getElementById("tbLoaiNguoiDung").innerText = "";
  document.getElementById("tbLoaiNgonNgu").innerText = "";
  document.getElementById("tbMoTa").innerText = "";
});

function showInforToForm(id) {
  document.getElementById("addBtn").style.display = "none";
  document.getElementById("editBtn").style.display = "block";
  document.getElementById("tbTaiKhoan").innerText = "";
  document.getElementById("tbHoTen").innerText = "";
  document.getElementById("tbMatKhau").innerText = "";
  document.getElementById("tbEmail").innerText = "";
  document.getElementById("tbHinhAnh").innerText = "";
  document.getElementById("tbLoaiNguoiDung").innerText = "";
  document.getElementById("tbLoaiNgonNgu").innerText = "";
  document.getElementById("tbMoTa").innerText = "";
  turnOnLoading();
  userService
    .getData(id)
    .then((res) => {
      turnOffLoading();
      $("#myModal").modal("show");
      document.querySelector("#userId").style.display = "block";
      document.querySelector("#userId span").innerHTML = res.data.id;
      document.getElementById("TaiKhoan").value = res.data.account;
      document.getElementById("HoTen").value = res.data.name;
      document.getElementById("MatKhau").value = res.data.password;
      document.getElementById("Email").value = res.data.email;
      document.getElementById("HinhAnh").value = res.data.picture;
      document.getElementById("loaiNguoiDung").value =
        res.data.userType == true ? "GV" : "HV";
      document.getElementById("loaiNgonNgu").value = res.data.language;
      document.getElementById("MoTa").value = res.data.description;
    })
    .catch((err) => {
      console.log(err);
    });
}
