function User(
  _account,
  _name,
  _password,
  _email,
  _picture,
  _type,
  _language,
  _description
) {
  this.account = _account;
  this.name = _name;
  this.password = _password;
  this.email = _email;
  this.picture = _picture;
  this.userType = _type;
  this.language = _language;
  this.description = _description;
}
