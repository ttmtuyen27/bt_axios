const BASE_URL = "https://6271e18725fed8fcb5ec0d4f.mockapi.io";

const userService = {
  getList: function () {
    return axios({
      url: `${BASE_URL}/user`,
      method: "GET",
    });
  },
  changeData: function (editUser, id) {
    return axios({
      url: `${BASE_URL}/user/${id}`,
      method: "PUT",
      data: editUser,
    });
  },
  deleteData: function (id) {
    return axios({
      url: `${BASE_URL}/user/${id}`,
      method: "DELETE",
    });
  },
  getData: function (id) {
    return axios({
      url: `${BASE_URL}/user/${id}`,
      method: "GET",
    });
  },
};
