const turnOnLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

const turnOffLoading = () => {
  document.getElementById("loading").style.display = "none";
};

function renderListOfUsers() {
  turnOnLoading();
  userService
    .getList()
    .then((res) => {
      turnOffLoading();
      outputUserInfor(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
}

function outputUserInfor(listUser) {
  let contentHTML = "";
  listUser.forEach((item) => {
    let contentTrTag = `
            <tr>
              <td>${item.id}</td>
              <td>${item.account}</td>
              <td>${item.password}</td>
              <td>${item.name}</td>
              <td>${item.email}</td>
              <td>${item.language}</td>
              <td>${item.userType == true ? "GV" : "HV"}</td>
              <td>
                <button class="btn btn-primary" onclick="showInforToForm(${
                  item.id
                })">Sửa</button>
                <button class="btn btn-danger" onclick="deleteUser(${
                  item.id
                })">Xóa</button>
              </td>
            </tr>
          `;
    contentHTML += contentTrTag;
  });
  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
}

function getInforFromForm() {
  let account = document.getElementById("TaiKhoan").value;
  let name = document.getElementById("HoTen").value;
  let password = document.getElementById("MatKhau").value;
  let email = document.getElementById("Email").value;
  let picture = document.getElementById("HinhAnh").value;
  let type = document.getElementById("loaiNguoiDung").value;
  let language = document.getElementById("loaiNgonNgu").value;
  let description = document.getElementById("MoTa").value;
  type = type == "GV" ? true : false;
  return new User(
    account,
    name,
    password,
    email,
    picture,
    type,
    language,
    description
  );
}

function validateInput(list) {
  let isValidName =
    validator.kiemTraRong("HoTen", "tbHoTen", "Họ tên không được để trống") &&
    validator.kiemTraTenHopLe();
  let isValidPassword = validator.kiemTraMatKhauHopLe();
  let isValidEmail = validator.kiemTraEmailHopLe();
  let isValidHinhAnh = validator.kiemTraRong(
    "HinhAnh",
    "tbHinhAnh",
    "Hình ảnh không được để trống"
  );
  let isValidNguoiDung = validator.kiemTraLuaChonHopLe(
    "loaiNguoiDung",
    "tbLoaiNguoiDung",
    "Loại người dùng không hợp lệ"
  );
  let isValidNgonNgu = validator.kiemTraLuaChonHopLe(
    "loaiNgonNgu",
    "tbLoaiNgonNgu",
    "Loại ngôn ngữ không hợp lệ"
  );
  let isValidMoTa =
    validator.kiemTraMoTaHopLe() &&
    validator.kiemTraRong("MoTa", "tbMoTa", "Mô tả không được để trống");
  let isValid =
    isValidEmail &&
    isValidHinhAnh &&
    isValidMoTa &&
    isValidName &&
    isValidNgonNgu &&
    isValidNguoiDung &&
    isValidPassword;
  return isValid;
}
