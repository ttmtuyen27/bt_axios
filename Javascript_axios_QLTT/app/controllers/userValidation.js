function Validation() {
  this.kiemTraRong = (idTarget, idError, message) => {
    let targetValue = document.getElementById(idTarget).value;
    if (!targetValue) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  this.kiemTaiKhoanHopLe = (list) => {
    let accountValue = document.getElementById("TaiKhoan").value;
    let index = list.findIndex((item) => {
      return item.account == accountValue;
    });
    console.log(index);
    if (index != -1) {
      document.getElementById("tbTaiKhoan").innerText = "Tài khoản đã tồn tại";
      return false;
    } else {
      document.getElementById("tbTaiKhoan").innerText = "";
      return true;
    }
  };
  this.kiemTraTenHopLe = () => {
    let name = document.getElementById("HoTen").value.trim();
    let pattern = /^[A-Za-z\s]*$/;
    if (!pattern.test(name)) {
      document.getElementById("tbHoTen").innerText = "Họ tên không hợp lệ";
      return false;
    } else {
      document.getElementById("tbHoTen").innerText = "";
      return true;
    }
  };
  this.kiemTraEmailHopLe = () => {
    let email = document.getElementById("Email").value;
    let pattern =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!pattern.test(email)) {
      document.getElementById("tbEmail").innerText = "Email không hợp lệ";
      return false;
    } else {
      document.getElementById("tbEmail").innerText = "";
      return true;
    }
  };
  this.kiemTraMatKhauHopLe = () => {
    let password = document.getElementById("MatKhau").value;
    let pattern =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,8}$/;
    if (!pattern.test(password)) {
      document.getElementById("tbMatKhau").innerText = "Mật khẩu không hợp lệ";
      return false;
    } else {
      document.getElementById("tbMatKhau").innerText = "";
      return true;
    }
  };
  this.kiemTraMoTaHopLe = () => {
    let description = document.getElementById("MoTa").value;
    let pattern = /[\s\S]{61,}/;
    if (pattern.test(description)) {
      document.getElementById("tbMoTa").innerText = "Mô tả vượt quá 60 từ";
      return false;
    } else {
      document.getElementById("tbMoTa").innerText = "";
      return true;
    }
  };
  this.kiemTraLuaChonHopLe = (idTarget, idError, message) => {
    let targetValue = document.getElementById(idTarget).value * 1;
    if (targetValue == -1 || targetValue == "") {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
}
